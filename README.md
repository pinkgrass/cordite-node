# cordite-node

1. Create **public** repo in gitlab (issue with private repos)
2. Operations -> Kubernetes -> Add Kubernetes Cluster
3. Sign in with Google -> Create new Kubernetes Cluster
4. Install Helm Tiller, Ingress, Prometheus, Gitlab Runner?
5. Settings -> CI/CD Settings -> Auto DevOps -> Select `Default to Auto DevOps pipeline` and Domain : <cluster public ip address>.nip.io
6. Settings -> CI/CD -> Variables -> Add `CONTAINER_SCANNING_DISABLED` ; `DEPENDENCY_SCANNING_DISABLED` ; `SAST_DISABLED` ; `CODE_QUALITY_DISABLED` ; `TEST_DISABLED` ; `PERFORMANCE_DISABLED` (value unimportant)
7. Add Dockerfile to repo



## gCloud client
1. Install gcloud client `brew cask install google-cloud-sdk` 
2. <missing auth>

## static IP address
gcloud compute addresses create cordite-node --region europe-west2
gcloud compute addresses describe cordite-node --region europe-west2

## Certificate
kubectl run letse --image linuxserver/letsencrypt --port 80 --env="EMAIL=richard@pinkgrass.org" --env="URL=35.242.154.193.nip.io" --env="SUBDOMAINS=cordite" --env="VALIDATION=http" --env="STAGING=true"
kubectl expose deployment letse --type LoadBalancer --port 80 --target-port 80 --load-balancer-ip=35.242.154.193
kubectl cp letse-579b74bf4d-ld5dl:etc/letsencrypt/archive/35.242.154.193.nip.io/fullchain1.pem cert.pem
kubectl cp letse-579b74bf4d-ld5dl:etc/letsencrypt/archive/35.242.154.193.nip.io/privkey1.pem key.pem
openssl dgst -sha256 -sign key.pem cert.pem \
    | base64 | cat cert.pem - \
    | curl -k -X POST -d @- https://nms-uat.cordite.foundation/certman/api/generate -o keys.zip
unzip keys.zip
kubectl create secret generic jks-cert --from-file=truststore.jks --from-file=nodekeystore.jks --from-file=sslkeystore.jks
kubectl delete deployment/letse
kubectl delete service/letse


## References
   + https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html  
   + https://cloud.google.com/sdk/docs/quickstart-macos  


## Interesting but in the end just a distraction
keytool -genkey -alias pinkgrass -keyalg RSA -keystore pinkgrass.jks -keysize 2048
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -nodes -verbose 
certbot certonly --standalone --test-cert --agree-tos -n -m richard@pinkgrass.org -d pinkgrass-cordite-node.35.242.154.193.nip.io
certbot certonly --manual --test-cert --config-dir . --work-dir . --logs-dir . --agree-tos -n -m richard@pinkgrass.org -d lb.1.2.3.4.nip.io

kubectl run nginx --image nginx --port 80 
kubectl expose deployment nginx --type LoadBalancer --port 80 --target-port 80 --load-balancer-ip=35.242.154.193 
